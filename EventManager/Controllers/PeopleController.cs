﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventManager.Models;

namespace EventManager.Models
{
    partial class Person
    {
        public string FullName => $"{FirstName} {LastName}";
    }
}

namespace EventManager.Controllers
{
    public class PeopleController : Controller
    {
        private EventsEntities db = new EventsEntities();

        // GET: People
        public ActionResult Index()
        {
            return View(db.Persons.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create(int? EventId)
            // lisatud väli, et ActionLink (EventDetails views) saaks anda Eventi numbri
            // välja tüüp on int? kuna see võib ka puududa (People/Index lehelt tulles)
        {
            ViewBag.EventId = EventId; // selle välja anname Create Viewle edasi
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FirstName,LastName")] Person person, int? EventId)
            // parameetrite lõppu pisasin int EventId, et lugeda saadetud vormilt see väli
        {
            if (ModelState.IsValid)
            {
                db.Persons.Add(person);
                db.SaveChanges();
                // kui person on andmebaasi lisatud, vaatan kas EventId on olemas
                // Kui on, siis tuleb LISAKS salvestatud uus person panna ka osalejate hulka
                // ning juhtimine anda selle Eventi Details lehele
                if (EventId.HasValue)
                {
                    db.Events.Find(EventId.Value)?.Persons.Add(person);
                    db.SaveChanges();
                    return RedirectToAction("Details", "Events", new { id = EventId });
                }

                return RedirectToAction("Index");
            }
            // juhul kui personi lisamine ei õnnestu, saadetakse ekraanile tagasi
            // create view - siis tuleb ka (olemasolev või puuduv) EventId sinna "tagasi" saata
            ViewBag.EventId = EventId;
            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.Persons.Find(id);
            db.Persons.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
